
FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y --no-install-recommends openssh-client curl zstd git python3 python3-pip && \
    pip3 install Jinja2 && \
    rm -rf /var/lib/apt/lists/*
